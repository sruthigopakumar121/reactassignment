import React from 'react';
import List from'./List'
import './App.css';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:[],
      text:'',
    }
    this.handleInput=this.handleInput.bind(this);
    this.addItem=this.addItem.bind(this);
  }
  handleInput=(e)=>{
    this.setState({
      text: e.target.value
    })
  }
  addItem=(e)=>
  {
      e.preventDefault();
      this.setState({
        text:'',
        items:[...this.state.items,this.state.text]
      });
  }
  onDelete = index=>{
    const deleteevent = [...this.state.items];
    deleteevent.splice(index,1);
    {
      this.setState({
        items:deleteevent
      })
    }

  }
  render(){
  return (
    <div className="App">
      <div className="Centerdiv">
        <br/>
        <h1>ToDo List</h1>
        <br/>
      </div>
      <header>
        
        <form id="todoform" >
          <input type ="text" placeholder="Enter the text" value={this.state.text} onChange={this.handleInput}/>
          <button onClick={this.addItem}>Add</button>
        </form>
      </header>
      <List deleteevent={this.onDelete} items={this.state.items}/>
    </div>
  );
}
}

export default App;
