import React from 'react';
import './List.css';
class List extends React.Component{
render(){
    return(
        <div>
            <ul>
                {this.props.items.map((item,index)=>(
                        <li key={index}>{item}&nbsp;&nbsp;
                            <span>&nbsp;
                             <button onClick={this.props.deleteevent.bind(this,index)}> X</button>
                             </span>

                        </li>
                ))}
                
            </ul>
        </div>
    )
}
}
export default List;